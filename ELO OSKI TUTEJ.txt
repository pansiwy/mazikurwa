CREATE TABLE uzytkownik (
 id serial PRIMARY KEY,
 typKonta varchar (50) NOT NULL,
  idKarty int(50),
 imie varchar(50),
nazwisko varchar(50),
email varchar(50) UNIQUE,
adres varchar(50),
login varchar(50) UNIQUE,
    haslo varchar(50)
);



CREATE TABLE rezerwacja (
 id serial PRIMARY KEY AUTO_INCREMENT,
    idUzytkownika integer NOT NULL REFERENCES uzytkownik (id) ON DELETE CASCADE,
  idWydarzenia integer NOT NULL REFERENCES wydarzenie (id) ON DELETE CASCADE,
  idMiejsca integer NOT NULL REFERENCES miejsce (id) ON DELETE CASCADE,
 idVouchera integer REFERENCES voucher (id) ON DELETE CASCADE,
    stanRezerwacji varchar(50),
    doZaplaty float
);

INSERT INTO uzytkownik VALUES(2,2342,'Organizator','Oskar','Dawid','odwaid@wp.pl','lukowa 3','odawid','31241');

CREATE TABLE wydarzenie (
 id serial PRIMARY KEY AUTO_INCREMENT,
  nazwa varchar(100),
    data varchar(50)
    );



CREATE TABLE voucher (
 id serial PRIMARY KEY,
  nazwa varchar(100),
    znizka float,
    kod varchar(50),
    stanvouchera varchar(50) NOT NULL
);


CREATE TABLE platnosc (
 id serial PRIMARY KEY,
 idRezerwacji integer NOT NULL REFERENCES rezerwacja (id) ON DELETE CASCADE,
    doZaplaty float,
    sposobPlatnosci varchar(50) NOT NULL,
    stanPlatnosci varchar(50) NOT NULL
);





CREATE TABLE miejsce (
 id serial PRIMARY KEY AUTO_INCREMENT,
 idWydarzenia integer NOT NULL REFERENCES wydarzenie (id) ON DELETE CASCADE,
     idSektora integer NOT NULL REFERENCES sektor (id) ON DELETE CASCADE,
    nrMiejsca integer(50) NOT NULL,
    cena float(50) NOT NULL,
    dostepnosc varchar(20)
);


CREATE TABLE Sektor (
 id serial PRIMARY KEY,
    nrSektora integer NOT NULL,
    typSektora varchar(50) NOT NULL
);

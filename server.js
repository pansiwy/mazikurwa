'use strict';
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const http = require('http').Server(app);

//MYSQL
const mysql = require('mysql');

 var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "admin",
  database: "bazdki"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ inflate: false }));

app.use('/css', express.static('css'));
app.use('/js', express.static('js'));
app.use('/img', express.static('img'));

app.use('/createEvent', function(req, res){
	var sql = "INSERT INTO wydarzenie (nazwa, data) VALUES ('"+req.body.name+"','"+req.body.date+"' )";
	con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("1 record inserted with id " + result.insertId);
	console.log(req.body);
	if(req.body.placesList){
		for (var i=0; i<req.body.placesList.length; ++i) {
  		var placeId = req.body.placesList[i];
			for (var l=1; l<=5; ++l) {
				var sql = "INSERT INTO miejsce (idWydarzenia, idSektora, nrMiejsca, cena, dostepnosc) VALUES ('"+result.insertId+"','"+placeId+"','"+l+"','"+req.body.price+"','tak')";
				con.query(sql, function (err, result) {
					if (err) throw err;
					console.log("1 record inserted");
				});
			}
		}
	}
	});

	res.send(200);
});

app.use('/activeUser', function(req, res){
	var sql1 = "SELECT * FROM uzytkownik";
	con.query(sql1, function (err, results) {
		if (err) {
			console.log(err)
			res.status(500).send({error:"nie mozna pobrac uzytkownika"});
		}
		else {
			console.log(results[0].login);
			res.send({ 'users': results});
		}
	});
});




app.use('/events', function(req, res){

	console.log(req.body);

	var sql = "SELECT * FROM wydarzenie";
	con.query(sql, function (err, results) {
    if (err) {
	console.log(err)
	res.status(500).send({error:"blad pobrania gowna z bazy"});
	}
	else {

	res.send({ 'events': results});
	}
  });
});

app.use('/seats', function(req, res){

	console.log(req.body);

	var sql = "SELECT * FROM miejsce";
	con.query(sql, function (err, results) {
    if (err) {
	console.log(err)
	res.status(500).send({error:"blad pobrania miejsc z bazy"});
	}
	else {

	res.send({ 'seats': results});
	}
  });
});
app.use('/sendSeats', function(req, res){
	//dostepnosc miejsc zmienic na nie null
  if (req.body.selectedSeats) {
	for(var i=0; i<req.body.selectedSeats.length; ++i){
	con.query("UPDATE miejsce SET dostepnosc = 'nie' WHERE id ="+req.body.selectedSeats[i]+";");
	//dodac rezerwacje na kazde miejsce
	con.query("INSERT INTO rezerwacja(idUzytkownika,idWydarzenia,idMiejsca,idVouchera,stanRezerwacji,doZaplaty) VALUES('1','"+req.body.event.id+"','"+req.body.selectedSeats[i]+"',123,'doZaplaty',200);")
	}
  }
	res.send();

});

app.use('/', function (req, res) {
    res.sendFile(__dirname  + '/home.html', {}, function(err) {
        if (err) {
            res.send({error: 'index not found'});
        }
    });
});

http.listen((process.env.PORT || 5001), function(){
    console.log('listening on *:5001');
});
